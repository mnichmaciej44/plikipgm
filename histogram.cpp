#include <fstream>
#include <iostream>
#include <string>
static int wymiaryGrafikiX;
static int wymiaryGrafikiY;
static int skala;




void getImageSize(std::string line){
  std::string sX="";
  std::string sY="";

  int i;
  for(i=0;line[i]!=32;i++){
    sX += line[i];
  }
  for(int j=i+1;line[j];j++){
    sY += line[j];
  }
  wymiaryGrafikiX = std::stoi(sX);
  wymiaryGrafikiY = std::stoi(sY);

}

void getImageScale(std::string line){
  skala = std::stoi(line);
}


void getMapa(std::string nazwa,int *tabi){
  std::fstream plik;
  plik.open(nazwa, std::ios::in | std::ios::app);
  std::string tab[wymiaryGrafikiX*wymiaryGrafikiY] = {""};
  if(plik.is_open()){
    std::string wiersz;
    //ustawienie kursora na początek obrazu
    for(int i =0;i<=3;i++) getline(plik,wiersz);
    int licznik=0;

      for(int i = 0;getline(plik,wiersz);i++){

      for(int j=0;j<wiersz.length();j++){
        if(wiersz[j]==32){
           licznik++;
         }else{
           tab[licznik]+=wiersz[j];

         }

       }
       licznik++;
}

plik.close();
}
for(int i=0;i<wymiaryGrafikiX*wymiaryGrafikiY;i++) {
  tabi[i] = std::stoi(tab[i]);
}

}


int main(int argc, char** argv){


std::string nazwa(argv[1]);
//walidacja parametrów

if(argc != 2){
  std::cerr << "Bledna liczba parametrow";
   return 1;
}
if(!(nazwa.substr(nazwa.length()-4,nazwa.length()-1) == ".pgm")){
  std::cerr << "Parametr 1 jest niewlasciwy";
  return 1;
}


//plik

std::ifstream plik(nazwa);
if(!plik){
  std::cerr << "Plik o takiej nazwie nie istnieje";
  return 1;
}else{
  if( plik.is_open())
  {
    std::string line;
    for(int i = 0;getline(plik,line);i++){
      if(i==2)getImageSize(line);
      else if(i==3)getImageScale(line);
    }

      plik.close();
  }else{
    std::cerr << "Nie mozna uruchomic pliku";
    return 1;
  }


}




  int mapa[wymiaryGrafikiX*wymiaryGrafikiY];

  getMapa(nazwa,mapa);
  int histogram[skala+1] = {0};

  for(int i=0;i<wymiaryGrafikiX*wymiaryGrafikiY;i++){
        histogram[mapa[i]]++;
}

for(int i =0; i< skala+1;i++){
  int ilosc =histogram[i];
  std::cout.width(6);
  std::cout<<i;
  std::cout<<": ";
  for(int j=0;j<ilosc;j++)std::cout<<"*";
std::cout<<"\n";
}

  return 0;
}
