Aplikacje umożliwiają edycje obrazów zapisanych w formacie pgm.
Każdy plik jest osobną aplikacją, która może działać niezależnie od pozostałych i należy go uruchamiać w konsoli podając odpowiednie parametry, np: stworz obraz.pgm 400 400 20 10.
Opis:

- stworz.exe

Parametry wywołania: stworz nazwa_pliku szerokość_obrazu wysokość_obrazu skala_szarości kolor_tła

Tworzy obraz o podanych wymiarach (w pikselach), skali szarości i kolorze tła. 



- rysuj.exe

Parametry wywołania: rysuj nazwa_pliku pozycja_początkowa_X pozycja_początkowa_Y długość_boku_rysowanego_kwadratu kolor_kwadratu tryb

Rysuje kwadrat o danej długości boku, zaczynając od miejscaw skazanego współrzędnymi X i Y. Parametr tryb przyjmuje wartość 0 lub 1. Dla trybu 0: zwykłe rysowanie kwadratu. Dla trybu 1: rysowanie kwadratów wraz z ustawieniem koloru tła na częściach wspólnych z innymi kwadratami.



- czysc.exe

Parametry wywołania: czysc nazwa_pliku kolor_wypełnienia

Wypełnia obraz jednolitym kolorem.


- histogram.exe

Parametry wywołania: histogram nazwa_pliku

Rysuje w konsoli histogram obrazu.