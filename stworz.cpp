#include <fstream>
#include <iostream>
#include <string>
int main(int argc, char** argv)
{
  if(argc!=6){
    std::cerr << "Bledna liczba parametrow";
     return 1;
}


    int szerokosc = atoi(argv[2]);
    int wysokosc = atoi(argv[3]);
    int skala = atoi(argv[4]);
    int kolor_tla = atoi(argv[5]);
    std::string nazwa(argv[1]);


    if(!(nazwa.substr(nazwa.length()-4,nazwa.length()-1) == ".pgm")){
      std::cerr << "Parametr 1 jest niewlasciwy";
      return 1;
    }

    if(szerokosc<1 || szerokosc > 1000){
        std::cerr << "Parametr 2 nie miesci sie w zakresie <1,100>";
        return 1;
    }

    if(wysokosc<1 || wysokosc > 1000){
        std::cerr<< "Parametr 3 nie miesci sie w zakresie <1,100>";
        return 1;
    }

    if(skala<1 || skala > 65536){
        std::cerr << "Parametr 4 nie miesci sie w zakresie <1,65536>";
        return 1;
    }

    if(kolor_tla<1 || kolor_tla > skala){
        std::cerr << "Parametr 5 nie miesci sie w zakresie <1,"<<skala<<">";
        return 1;
    }

    std::fstream plik(argv[1]);
    if(plik){
      std::cerr << "Plik o takiej nazwie juz istnieje";
      return 1;
    }else{
      plik.open( argv[1], std::ios::out );
      if( plik.good() == true )
      {
        plik << "P2\r\n"<<"# "<<argv[1]<<"\r\n"<<szerokosc<<" "<<wysokosc<<"\r\n"<<skala<<"\r\n";
        for(int i=0;i<wysokosc;i++){
          for(int j=0;j<szerokosc;j++){
            plik << kolor_tla;
            if(j!=szerokosc-1)plik << " ";
          }
          plik<<"\r\n";
        }
          plik.close();

      }
    }

    return 0 ;
}
