#include <fstream>
#include <iostream>
#include <string>
static int wymiaryGrafikiX;
static int wymiaryGrafikiY;
static int skala;



//funkcje



void getImageSize(std::string line){
  std::string sX="";
  std::string sY="";

  int i;
  for(i=0;line[i]!=32;i++){
    sX += line[i];
  }
  for(int j=i+1;line[j];j++){
    sY += line[j];
  }
  wymiaryGrafikiX = std::stoi(sX);
  wymiaryGrafikiY = std::stoi(sY);

}

void getImageScale(std::string line){
  skala = std::stoi(line);
}

bool walidacjaParametrow(int pozycjaX,int pozycjaY,int dlugoscBoku, int kolor, int tryb){


  if(pozycjaX<0 || pozycjaX > wymiaryGrafikiX){
    std::cerr << "Parametr 2 nie miesci sie w zakresie <0,"<<wymiaryGrafikiX<<">";
    return 1;
  }


  if(pozycjaY<0 || pozycjaY > wymiaryGrafikiY){
    std::cerr << "Parametr 3 nie miesci sie w zakresie <0,"<<wymiaryGrafikiY<<">";
    return 1;
  }
  if(dlugoscBoku<1 || dlugoscBoku > wymiaryGrafikiX || dlugoscBoku > wymiaryGrafikiY){
    int dlg = (wymiaryGrafikiX > wymiaryGrafikiY) ? wymiaryGrafikiY : wymiaryGrafikiX;
    std::cerr << "Parametr 4 nie miesci sie w zakresie <1,"<<dlg<<">";
    return 1;
  }

  if(kolor<0 || kolor > skala){
    std::cerr << "Parametr 5 nie miesci sie w zakresie <0,"<<skala<<">";
    return 1;
  }
  if(tryb!=0 && tryb !=1){
    std::cerr << "Parametr 6 powinien przyjmowac wartosc 0 lub 1";
    return 1;
  }

  if(dlugoscBoku+pozycjaX+1 > wymiaryGrafikiX ||dlugoscBoku+pozycjaY > wymiaryGrafikiY){
    std::cerr << "Ten kwadrat wykracza poza wymiary obrazu";
    return 1;
  }
  return 0;
}

void getMapa(std::string nazwa,int *tabi){
  std::fstream plik;
  plik.open(nazwa, std::ios::in | std::ios::app);
  std::string tab[wymiaryGrafikiX*wymiaryGrafikiY] = {""};

  if(plik.is_open()){
    std::string wiersz;
    //ustawienie kursora na początek obrazu
    for(int i =0;i<=3;i++) getline(plik,wiersz);
    int licznik=0;

      for(int i = 0;getline(plik,wiersz);i++){

      for(int j=0;j<wiersz.length();j++){
        if(wiersz[j]==32){
           licznik++;
         }else{
           tab[licznik]+=wiersz[j];
         }

       }
       licznik++;
}
}
for(int i=0;i<wymiaryGrafikiX*wymiaryGrafikiY;i++) tabi[i] = std::stoi(tab[i]);

}

int histogram(int * mapa){
int kt;

  int kttab[skala+1] = {0};
  for(int i=0;i<wymiaryGrafikiX*wymiaryGrafikiY;i++) kttab[mapa[i]]++;

   int j=0;
   for(int i=0;i<=skala;i++){
     if(j<kttab[i]){
       j = kttab[i];
       kt=i;
     }
   }

return kt;
}

void rysuj(std::string nazwa,int pozycjaX,int pozycjaY,int dlugoscBoku,int kolor,int tryb,int *mapa){
int kt;
std::fstream plik;
if(tryb){
  kt = histogram(mapa);
}
plik.open(nazwa, std::ios::in | std::ios::app);
std::ofstream fileout("tmp"+nazwa); //Temporary file

if(plik.is_open()){
  std::string wiersz;
  //ustawienie kursora na początek obrazu
  for(int i =0;i<=3;i++){
    getline(plik,wiersz);
    fileout<<wiersz<<"\n";
  }
  //
  std::string tempwiersz[wymiaryGrafikiX];

  for(int y=1;y<=wymiaryGrafikiY;y++){
    for(int x=1;x<=wymiaryGrafikiX;x++){

      if(y-1>=pozycjaY && y-1<pozycjaY+dlugoscBoku && x-1>=pozycjaX && x-1<pozycjaX+dlugoscBoku) {
        if(tryb){
          if(mapa[((y-1)*10)+x-1]==kt)
          fileout<<kolor;
          else fileout<<kt;
        }else fileout<<kolor;
      }

      else fileout<<mapa[((y-1)*10)+x-1];
      if(x!=wymiaryGrafikiX) fileout<<" ";

    }
    fileout<<"\n";

  }


}
plik.close();
fileout.close();

std::fstream tmp;
tmp.open("tmp"+nazwa, std::ios::in | std::ios::app);
std::ofstream plik2(nazwa);
std::string wiersz;
if(tmp.is_open())
for(int i=0;i<=wymiaryGrafikiY+3;i++){
  getline(tmp,wiersz);
  plik2<< wiersz+"\n";
}

tmp.close();
plik2.close();
std::string nazwachar = "tmp"+nazwa;


remove( &nazwachar[0] ) != 0;



}




//main
int main(int argc, char** argv){

int pozycjaX = atoi(argv[2]);
int pozycjaY = atoi(argv[3]);
int dlugoscBoku = atoi(argv[4]);
int kolor = atoi(argv[5]);
int tryb = atoi(argv[6]);

std::string nazwa(argv[1]);
//walidacja parametrów

if(argc != 7){
  std::cerr << "Bledna liczba parametrow";
   return 1;
}
if(!(nazwa.substr(nazwa.length()-4,nazwa.length()-1) == ".pgm")){
  std::cerr << "Parametr 1 jest niewlasciwy";
  return 1;
}


//plik

std::ifstream plik(nazwa);
if(!plik){
  std::cerr << "Plik o takiej nazwie nie istnieje";
  return 1;
}else{
  if( plik.is_open())
  {
    std::string line;
    for(int i = 0;getline(plik,line);i++){
      if(i==2)getImageSize(line);
      else if(i==3)getImageScale(line);
    }

      plik.close();
  }else{
    std::cerr << "Nie mozna uruchomic pliku";
    return 1;
  }


}
// walidaczja cz2

if(walidacjaParametrow(pozycjaX,pozycjaY,dlugoscBoku,kolor,tryb)) return 1;

  int mapa[wymiaryGrafikiX*wymiaryGrafikiY];
  getMapa(nazwa,mapa);


  rysuj(nazwa,pozycjaX,pozycjaY,dlugoscBoku,kolor,tryb,mapa);


  return 0;
}
