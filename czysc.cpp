#include <fstream>
#include <iostream>
#include <string>
static int wymiaryGrafikiX;
static int wymiaryGrafikiY;
static int skala;

void getImageSize(std::string line){
  std::string sX="";
  std::string sY="";

  int i;
  for(i=0;line[i]!=32;i++){
    sX += line[i];
  }
  for(int j=i+1;line[j];j++){
    sY += line[j];
  }
  wymiaryGrafikiX = std::stoi(sX);
  wymiaryGrafikiY = std::stoi(sY);

}

void getImageScale(std::string line){
  skala = std::stoi(line);
}


bool walidacjaParametrow(int kolor){

  if(kolor<0 || kolor > skala){
    std::cerr << "Parametr 5 nie miesci sie w zakresie <0,"<<skala<<">";
    return 1;
  }
  return 0;
}

std::string getPoczatekPliku(std::string nazwa){
  std::ifstream plik(nazwa);
  std::string poczatek="";
  std::string line;
  for(int i = 0;getline(plik,line),i<=3;i++){
    if(i!=0)poczatek+="\n";
    poczatek+=line;
  }
  return poczatek;
}

int main(int argc, char** argv){

  if(argc != 3){
    std::cerr << "Bledna liczba parametrow";
     return 1;
  }
  std::string nazwa(argv[1]);

  if(!(nazwa.substr(nazwa.length()-4,nazwa.length()-1) == ".pgm")){
    std::cerr << "Parametr 1 jest niewlasciwy";
    return 1;
  }


int kolor = atoi(argv[2]);


std::ifstream plik(nazwa);
if(!plik){
  std::cerr << "Plik o takiej nazwie nie istnieje";
  return 1;
}else{
  if( plik.is_open())
  {
    std::string line;
    for(int i = 0;getline(plik,line);i++){
      if(i==2)getImageSize(line);
      else if(i==3)getImageScale(line);
    }

      plik.close();
  }else{
    std::cerr << "Nie mozna uruchomic pliku";
    return 1;
  }


}
if(walidacjaParametrow(kolor)) return 1;

std::string poczatekPliku = getPoczatekPliku(nazwa);

std::ofstream plik2(nazwa);
plik2<<poczatekPliku<<"\n";

for(int y=0;y<wymiaryGrafikiY;y++){
  for(int x=0;x<wymiaryGrafikiX;x++){
      plik2<<kolor;
      if(x!=wymiaryGrafikiX-1) plik2<<" ";
  }
  if(y!=wymiaryGrafikiY-1)plik2<<"\n";
}
plik.close();


return 0;
}
